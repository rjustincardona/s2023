from graph import *

f_str = "x0*x1 + x1"
f_opt = sympy.sympify(f_str)
res_num = len(f_opt.free_symbols)
syms = [i for i in f_opt.free_symbols]

def make_2d_grid(N):
    graph = Graph()

    def mesh2d(x, y):
        return np.transpose([np.tile(x, len(y)), np.repeat(y, len(x))])
        
    mesh = mesh2d(np.arange(N), np.arange(N))
    for m in mesh:
        graph.add_vertex([np.random.rand() for i in range(res_num)], np.random.rand(), [np.random.rand() for i in range(res_num)], f_opt, [], m)
        # graph.add_vertex([np.random.rand() for i in range(res_num)], np.random.rand(), f_opt, f_opt, [], m)
    for n in graph.vertices:
        id_num = n.id_num
        col = id_num % N
        row = id_num - col
        l = row + ((col - 1) % N)
        r = row + ((col + 1) % N)
        t = ((row - N) % (N**2)) + col
        b = ((row + N) % (N**2)) + col
        for i in [l, r, b, t]:
            n.neighbours.append(i)         
    return graph

graph = make_2d_grid(10)
graph.plot_graph('animation', val_type = 0)
