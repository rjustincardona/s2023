import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib import cm
import sympy

def optimize(vertex):
    s = np.sum(vertex.power_exp)
    for i in range(len(vertex.res)):
        vertex.res[i] = vertex.power * vertex.power_exp[i] / s

class Vertex:
    def __init__(self, id_num, res_cap, power, power_exp, neighbours=[], coord=None, opt_exp = lambda x:x):
        '''
        Initializes an instance of the Vertex class.

        Parameters
        ----------
        id_num: int
            The ID number of the vertex
        res_cap: array of float
            The maximal capacity of the vertex for each resource
        power: float
            The power consumption of the vertex
        neighbours: array of int
            the neighbours of the vertex
        coord: array of float
            The coordinates of the vertex in the ambient space

        Returns
        -------
        A Vertex object with the initialized parameters and the resource utilization of the previous 2 time steps
        '''
        self.id_num = id_num
        self.res_cap = res_cap
        self.power = power
        self.powerp = power
        self.powerpp = power
        self.neighbours = neighbours
        self.coord = coord
        self.opt_exp = opt_exp
        self.power_exp = power_exp
        self.res = res_cap
        optimize(self)


class Graph:
    def __init__(self):
        '''
        Initializes an instance of the Graph class.
        '''
        self.vertices = []
        self.id_cur = 0

    def get_vertex(self, id_num):
        '''
        Parameters
        ----------
        id_num: int
            ID number of the vertex to be obtained

        Returns
        -------
        The first vertex with the supplied ID
        '''
        return [n for n in self.vertices if n.id_num == id_num][0]

    def add_vertex(self, res_cap, power, power_exp, opt_exp, neighbours=[], coord=None):
        '''
        Parameters
        ----------
        res_cap: array of float
            The maximal capacity of the vertex for each resource
        power: float
            The power consumption of the vertex
        neighbours: array of int
            the neighbours of the vertex
        coord: array of float
            The coordinates of the vertex in the ambient space

        Returns
        -------
        None
            Adds a vertex with the desired parameters to the graph
        '''
        temp_vertex = Vertex(self.id_cur, res_cap=res_cap, power=power, power_exp=power_exp, neighbours=neighbours, coord=coord, opt_exp=opt_exp)
        self.vertices.append(temp_vertex)
        for n in neighbours:
            self.get_vertex(n).neighbours.append(self.id_cur)
        self.id_cur += 1

    def remove_node(self, id_num):
        '''
        Parameters
        ----------
        id_num: int
            ID of the node to be removed

        Returns
        -------
        None
            Removes the node with the specified ID from the graph
        '''
        temp_vertex = self.get_vertex(id_num)
        self.vertices.remove(temp_vertex)
        for n in vertex_temp.neighbours:
            self.get_vertex(n).neighbours.remove(id_num)

    def get_edges(self):
        '''
        Parameters
        ----------
        None

        Returns
        -------
        edges: array like
            The list of edges in the graph            
        '''
        edges = []
        for i in self.vertices:
            for j in i.neighbours:
                edge_temp = [i.id_num, j]
                if (edge_temp in edges) == False and (edge_temp.reverse() in edges) == False:
                    edges.append(edge_temp)
        return edges

    def update(self, vertex, m=0, a=-1e-1, t=1, eps=0):
            p = vertex.power
            pp = vertex.powerp
            ppp = vertex.powerpp
            c = vertex.coord
            neighbours = vertex.neighbours
            points = []
            for i in range(len(neighbours)):
                v_temp = self.get_vertex(neighbours[i])
                r = v_temp.coord - np.array(c)
                r = np.hypot(*r)
                z = v_temp.power - p
                points.append([r, z])
            if len(points) % 2 != 0:
                points.append([0, 0])
            dx2 = 0
            for i in range(0, len(points), 2):
                try:
                    dx2 += 2 * np.polyfit([0, points[i][0], points[i+1][0]], [0, points[i][1], points[i+1][1]], 2)[0]
                except:
                    continue

            new_val = ((2 - (m * t)**2) * p - ((1 + (a/2)) * pp) + (dx2 * (t**2))) / (1 - (a/2))
            vertex.powerp = p
            vertex.powerpp = pp
            if new_val > 1:
                vertex.power = 1 - eps
            elif new_val < 0:
                vertex.power = eps
            else:
                vertex.power = new_val
            optimize(vertex)

    def plot_graph(self, option='2d', val_type='power'):
        '''
        Parameters
        ----------
        option: string
            Specifies the type of graph to be created. This can be '2d', '3d', or 'anim'.
        Returns
        -------
        None
            Makes a 2d, 3d, or animated plot depending on the supplied parameter
        '''
        if option=='2d':
            edges = self.get_edges()
            coords = [i.coord for i in self.vertices]
            edge_coords = [[self.get_vertex(i).coord for i in j] for j in edges]
            for i in coords:
                plt.scatter(i[0], i[1], s=60., linewidths=0.5, c="#61D836", edgecolors="black", zorder=1)
            for i in edge_coords:
                i  = np.array(i)
                plt.plot(i.T[0], i.T[1], color="black", linewidth=1, alpha=0.2, zorder=0)
            plt.show()

        if option=='3d':
            coords = np.array([i.coord for i in self.vertices])
            vals = np.array([i.power for i in self.vertices])
            coords = np.append(coords.T, [vals], axis=0)
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            surf = ax.plot_trisurf(coords[0], coords[1],  coords[2], cmap=cm.jet, linewidth=0)
            fig.colorbar(surf)
            fig.tight_layout()
            plt.show()

        elif option=='animation':
            coords = np.array([i.coord for i in self.vertices])
            if val_type == 'power':
                vals = np.array([i.power for i in self.vertices])
            else:
                vals = np.array([i.res[val_type] for i in self.vertices])
            coords = np.append(coords.T, [vals], axis=0)
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            def animate(n):
                ax.cla()
                for n in self.vertices:
                    self.update(n)
                coords = np.array([i.coord for i in self.vertices])
                if val_type == 'power':
                    vals = np.array([i.power for i in self.vertices])
                else:
                    vals = np.array([i.res[val_type] for i in self.vertices])
                coords = np.append(coords.T, [vals], axis=0)
                ax.plot_trisurf(coords[0], coords[1], coords[2], cmap=cm.jet, linewidth=0)
                return fig,
            anim = FuncAnimation(fig = fig, func = animate, frames = 300, interval = 1, repeat = False)
            anim.save("animation.gif",fps=24)


